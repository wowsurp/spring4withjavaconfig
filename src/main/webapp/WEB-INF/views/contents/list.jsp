<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h2>게시판</h2>
<button type="button" onclick="location.href='/excelDownload'">엑셀 다운로드</button>
<table>
	<tr>
		<td>제목</td>
		<td>내용</td>
		<td>작성자</td>
	</tr>
	<c:forEach items="${contentsList}" var="contents">
	<tr>
		<td>
			<a href="/contents/detail?idx=${contents.idx}">${contents.title}</a>
		</td>
		<td>${contents.contents}</td>
		<td>${contents.userNm}</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>