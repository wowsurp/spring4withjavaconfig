<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>

<t:insertAttribute name="header"></t:insertAttribute>
<hr>
<t:insertAttribute name="content"></t:insertAttribute>
<div style="position:fixed;left:0;bottom:0;width:100%;height:100px;background:gray;color:white;">
	<t:insertAttribute name="footer"></t:insertAttribute>
</div>