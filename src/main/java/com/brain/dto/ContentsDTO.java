package com.brain.dto;

import java.sql.Date;

public class ContentsDTO {
	
	private int idx;
	private String title;
	private String contents;
	private String userNm;
	private String userId;
	private Date regDtm;
	private String modifyUserId;
	private Date modifyDtm;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getRegDtm() {
		return regDtm;
	}
	public void setRegDtm(Date regDtm) {
		this.regDtm = regDtm;
	}
	public String getModifyUserId() {
		return modifyUserId;
	}
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	public Date getModifyDtm() {
		return modifyDtm;
	}
	public void setModifyDtm(Date modifyDtm) {
		this.modifyDtm = modifyDtm;
	}
}