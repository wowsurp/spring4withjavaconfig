package com.brain.dao;

import java.util.List;

import com.brain.dto.ContentsDTO;

public interface ContentsDAO {
	public List<ContentsDTO> selectContentsList() throws Exception;
	public ContentsDTO selectContents(int idx);
}
