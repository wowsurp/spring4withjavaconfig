package com.brain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.brain.dto.ContentsDTO;

@Repository
public class ContentsDAOImpl implements ContentsDAO{

	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public ContentsDTO selectContents(int idx) {
		return sqlSession.selectOne("com.brain.mapper.contentsMapper.selectContents", idx);
	}

	@Override
	public List<ContentsDTO> selectContentsList() throws Exception {
		return sqlSession.selectList("com.brain.mapper.contentsMapper.selectContentsList");
	}

}
