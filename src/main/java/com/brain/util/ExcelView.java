package com.brain.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.brain.dto.ContentsDTO;

public class ExcelView extends AbstractExcelView{

	@Override
	protected Workbook createWorkbook() {
		return new SXSSFWorkbook();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) {
		
		List<ContentsDTO> contentsList = (List<ContentsDTO>) model.get("excelList");
		
		Sheet sheet = workbook.createSheet("게시판");
		Row row = null;
		int rowCount = 0;
		int cellCount = 0;
		
		row = sheet.createRow(rowCount++);
		
		row.createCell(cellCount++).setCellValue("제목");
		row.createCell(cellCount++).setCellValue("내용");
		row.createCell(cellCount++).setCellValue("작성자");
		
		for(ContentsDTO contents : contentsList) {
			row = sheet.createRow(rowCount++);
			cellCount = 0;
			row.createCell(cellCount++).setCellValue(contents.getTitle());
			row.createCell(cellCount++).setCellValue(contents.getContents());
			row.createCell(cellCount++).setCellValue(contents.getUserNm());
		}
	}

}
