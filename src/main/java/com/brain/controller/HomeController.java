package com.brain.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) throws Exception {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		String path = URLEncoder.encode("C:\\Users\\user\\Documents\\workspace-sts-3.9.6.RELEASE\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\sample\\resources\\files", "UTF-8");
//		String path = "C:\\Users\\user\\Documents\\workspace-sts-3.9.6.RELEASE\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\sample\\resources\\files";
		String fileName= "1fb2518d-feeb-432f-a32b-d4ef028f3a91_20181217_171928_163.jpg";
		
		model.addAttribute("serverTime", formattedDate );
		model.addAttribute("path", path);
		model.addAttribute("fileName", fileName);
		
		return "home.t";
	}
	
}
