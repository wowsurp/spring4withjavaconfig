package com.brain.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FileController {
	
	private static final String UPLOAD_PATH = "resources" + File.separator + "files";

	@RequestMapping("/fileupload")
	public String upload(@RequestParam("uploadfile") MultipartFile uploadFile, 
													HttpServletRequest request, Model model) throws IOException {
		String saveName = saveFile(request, uploadFile);
		
		model.addAttribute("saveName", saveName);
		
		return "upload.t";
		
	}
	
	private String saveFile(HttpServletRequest request, MultipartFile file) {
		
		UUID uuid = UUID.randomUUID();
		
		String filePath = request.getSession().getServletContext().getRealPath("/") + UPLOAD_PATH;
		String saveName = uuid + "_" + file.getOriginalFilename();
		
		File saveFile = new File(filePath, saveName);
		
		try {
			file.transferTo(saveFile);
		}catch(IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return saveName;
	}
	
	@RequestMapping("/filedownload")
	public ModelAndView download(@RequestParam("path") String path,
								@RequestParam("fileName") String fileName) {
		
		String fullPath = path + "\\" + fileName;
		File file = new File(fullPath);
		
		return new ModelAndView("fileDownload", "downloadFile", file);
	}
}
