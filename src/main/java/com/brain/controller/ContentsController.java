package com.brain.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.brain.dto.ContentsDTO;
import com.brain.service.ContentsService;

@Controller
public class ContentsController {
	
	@Autowired
	private ContentsService service;
	
	
	@RequestMapping(value = "/contents", method = RequestMethod.GET)
	public String contents(Model model) {
		
		ContentsDTO contents = service.selectContents(8);
		
		model.addAttribute("contents", contents);
		
		return "contents.t";
	}
	
	@RequestMapping("/upload")
	public String upload() {
		return "upload.t";
	}
	
	@RequestMapping("/contents/list")
	public String home(Locale locale, Model model) throws Exception {
	List<ContentsDTO> contentsList = service.selectContentsList();
	
	model.addAttribute("contentsList", contentsList);
	
	return "contents/list.t";
	}
}
