package com.brain.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.brain.dto.ContentsDTO;
import com.brain.service.ContentsService;

@Controller
public class ExcelController {
	
	@Autowired
	private ContentsService service;
	
	@RequestMapping("/excelDownload")
	public String excelTransform(Model model, HttpServletResponse res) throws Exception {
		
		res.setHeader("Content-disposition", "attachment; filename=excel.xlsx");
		
		List<ContentsDTO> excelList = service.selectContentsList();
		
		model.addAttribute("excelList", excelList);
		
		return "excelView";
	}
}
