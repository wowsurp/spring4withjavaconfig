package com.brain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brain.dao.ContentsDAO;
import com.brain.dto.ContentsDTO;

@Service
public class ContentsServiceImpl implements ContentsService{

	@Autowired
	private ContentsDAO dao;
	
	@Override
	public ContentsDTO selectContents(int idx) {
		return dao.selectContents(idx);
	}

	@Override
	public List<ContentsDTO> selectContentsList() throws Exception {
		return dao.selectContentsList();
	}
}
