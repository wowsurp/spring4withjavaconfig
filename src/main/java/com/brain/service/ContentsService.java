package com.brain.service;

import java.util.List;

import com.brain.dto.ContentsDTO;

public interface ContentsService {
	public List<ContentsDTO> selectContentsList() throws Exception;
	public ContentsDTO selectContents(int idx);
}
